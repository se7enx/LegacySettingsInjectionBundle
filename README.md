# Legacy Settings Injection Bundle
This is an eZ Publish 5 Symfony bundle to inject settings from a yml file into the legacy eZ Publish kernel.

It allows injecting different legacy settings for different siteaccesses.

## Installation
1. Run `composer require`:
    ```bash
    $ composer require contextualcode/legacy-settings-injection-bundle
    ```
2. Enable this bundle in `app/AppKernel.php` (`ezpublish/EzPublishKernel.php`) file by adding next line in `registerBundles` method:
    ```php
       public function registerBundles()
       {
           $bundles = array(
               ...
               new ContextualCode\LegacySettingsInjectionBundle\ContextualCodeLegacySettingsInjectionBundle()
           );
    ```

## Usage
There are two cases for this bundle usage.
### Inject Legacy Settings for all Siteaccces
In your `parameters.yml` file, add legacy settings like:
```yml
parameters:
    injected_ini_files:
        site.ini:
            TemplateSettings:
                Debug: enabled
```
### Inject Legacy Settings for specific Siteaccces
In your `config.yml` file, add legacy settings for different siteaccesses like:
```yml
contextual_code_legacy_settings_injection:
    system:
        frontend_group:
            injected_ini_files:
                site.ini:
                    TemplateSettings:
                        Debug: disabled
        admin:
            injected_ini_files:
                site.ini:
                    TemplateSettings:
                        Debug: enabled
```
