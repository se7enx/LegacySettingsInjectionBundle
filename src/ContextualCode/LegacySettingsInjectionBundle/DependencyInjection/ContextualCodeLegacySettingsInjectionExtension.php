<?php

namespace ContextualCode\LegacySettingsInjectionBundle\DependencyInjection;

use eZ\Bundle\EzPublishCoreBundle\DependencyInjection\Configuration\SiteAccessAware\ConfigurationProcessor;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ContextualCodeLegacySettingsInjectionExtension extends Extension
{
    /**
     *{@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container) {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader(
            $container, new FileLocator(__DIR__ . '/../Resources/config')
        );
        $loader->load('parameters.yml');
        $loader->load('services.yml');
        $loader->load('default.yml');

        $processor = new ConfigurationProcessor($container, 'contextual_code_legacy_settings_injection');
        $processor->mapSetting('injected_ini_files', $config);
    }
}

