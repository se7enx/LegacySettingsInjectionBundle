<?php

namespace ContextualCode\LegacySettingsInjectionBundle\DependencyInjection;

use eZ\Bundle\EzPublishCoreBundle\DependencyInjection\Configuration\SiteAccessAware\Configuration as SiteAccessConfiguration;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration extends SiteAccessConfiguration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('contextual_code_legacy_settings_injection');
        $rootNode->ignoreExtraKeys();

        $systemNode = $this->generateScopeBaseNode($rootNode);
        $systemNode
            ->variableNode('injected_ini_files')
            ->end();
        return $treeBuilder;
    }
}
