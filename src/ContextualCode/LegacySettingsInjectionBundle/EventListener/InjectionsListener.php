<?php

namespace ContextualCode\LegacySettingsInjectionBundle\EventListener;

use eZ\Publish\Core\MVC\Legacy\LegacyEvents;
use eZ\Publish\Core\MVC\Legacy\Event\PreBuildKernelEvent;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class InjectionsListener implements EventSubscriberInterface
{
    public static $settings = null;
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function onBuildKernel(PreBuildKernelEvent $event) {
        $alreadyInjectedSettings = (array) $event->getParameters()->get('injected-settings');
        $settings = $this->getInjectedSettings() + $alreadyInjectedSettings;
        $event->getParameters()->set('injected-settings', $settings);
    }

    protected function getInjectedSettings() {
        if (self::$settings === null) {
            self::$settings = $this->fetchInjectedSettings();
        }

        return self::$settings;
    }

    protected function fetchInjectedSettings() {
        $settings = array();
        $iniFiles = $this->getInjectedLegacyIniFiles();
        if (is_array($iniFiles) === false) {
            return $settings;
        }

        foreach ($iniFiles as $iniFile => $iniFileSettings) {
            foreach ($iniFileSettings as $block => $blockSettings) {
                foreach ($blockSettings as $var => $val) {
                    if (isset($settings["$iniFile/$block/$var"])) {
                        if (!is_array($settings["$iniFile/$block/$var"])) {
                            $settings["$iniFile/$block/$var"] = array($settings["$iniFile/$block/$var"]);
                        }
                        if (!is_array($val)) {
                            $val = array($val);
                        }
                        $settings["$iniFile/$block/$var"] = array_merge($settings["$iniFile/$block/$var"], $val);
                    } else {
                        $settings["$iniFile/$block/$var"] = $val;
                    }
                }
            }
        }

        return $settings;
    }

    protected function getInjectedLegacyIniFiles()
    {
        $container = $this->container;
        $configResolver = $container->get('ezpublish.config.resolver');
        $siteAccessParam = array(
            'injected_ini_files',
            'contextual_code_legacy_settings_injection'
        );
        if (
            $configResolver->hasParameter(
                $siteAccessParam[0],
                $siteAccessParam[1]
            )
        ) {
            $siteAccessSettings = (array) $configResolver->getParameter(
                $siteAccessParam[0],
                $siteAccessParam[1]
            );
        }
        else {
            $siteAccessSettings = array();
        }

        $globalSettings = array();
        if ($container->hasParameter('ezpublish_legacy_ini')) {
            $globalSettings = (array) $container->getParameter('ezpublish_legacy_ini');
        } elseif($container->hasParameter('injected_ini_files')) {
            $globalSettings = (array) $container->getParameter('injected_ini_files');
        }

        return array_merge($globalSettings, $siteAccessSettings);
    }

    public static function getSubscribedEvents()
    {
        return array(
            LegacyEvents::PRE_BUILD_LEGACY_KERNEL => array('onBuildKernel', 128),
        );
    }
}

